# Copyright © 2018 Carl Chenet <carl.chenet@ohmytux.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Setup for TaskTask
'''Setup for TaskTask'''

from setuptools import setup, find_packages

CLASSIFIERS = [
    'Intended Audience :: System Administrators',
    'Environment :: Console',
    'License :: OSI Approved :: GNU General Public License (GPL)',
    'Operating System :: POSIX :: Linux',
    'Programming Language :: Python :: 3.5',
    'Programming Language :: Python :: 3.6'
]

setup(
    name='tasktask',
    version='0.1',
    license='GNU GPL v3',
    description='Simple task scheduler',
    long_description='TaskTask is a simple task scheduler',
    author = 'Carl Chenet',
    author_email = 'chaica@ohmytux.com',
    url = 'https://gitlab.com/chaica/tasktask',
    classifiers=CLASSIFIERS,
    download_url='https://gitlab.com/chaica/tasktask',
    packages=find_packages(),
    scripts=['scripts/tasktask'],
    install_requires=['sqlalchemy'],
    extras_require={
        'mysql':  ['mysql_connector'],
        'postgresql':  ['psycopg2'],
    }
)
