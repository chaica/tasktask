# tasktask

TaskTask is a simple task scheduler.

### Quick Install

Install tasktask from PyPI

        # pip3 install tasktask

Install tasktask from sources

        # tar zxvf tasktask-0.1.tar.gz
        # cd tasktask
        # python3 setup.py install
        # # or
        # python3 setup.py install --install-scripts=/usr/bin

### Configure tasktask

Create or modify tasktask.ini file in order to configure tasktask:

        [database]
        name=task
        user=task
        pass=V3rYS3cR3tP4sSw0rd,
        
For readability some parameters are not in the file, so their defaut values are used. [Read the configure section](https://tasktask.readthedocs.org/en/latest/configure.html) of the official documentation for the default value of the different parameters.

### Use tasktask

Launch tasktask

        $ tasktask -c /path/to/tasktask.ini

### Authors

Carl Chenet <chaica@ohmytux.com>

### License

This software comes under the terms of the GPLv3+. See the LICENSE file for the complete text of the license.
