# -*- coding: utf-8 -*-
# Copyright © 2018 Carl Chenet <carl.chenet@ohmytux.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/

# Get values of the configuration file for the Redis server
'''Get values of the configuration file for the Redis server'''

def parse_database(config):
   # database section
    section = 'database'
    databaseconf = {}
    databaseconf['connector'] = 'mysql+mysqlconnector'
    databaseconf['host'] = 'localhost'
    databaseconf['port'] = 3306
    databaseconf['sql_query'] = "select id, schedule, created, command from {database} order by id"
    if config.has_section(section):
        for parameter in ('connector', 'host', 'port', 'user', 'pass', 'name', 'sql_query'):
            if config.has_option(section, parameter):
                databaseconf[parameter] = config.get(section, parameter)
    return databaseconf
