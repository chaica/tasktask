# Copyright © 2018 Carl Chenet <carl.chenet@ohmytux.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

'''Extract the users'''

# 3rd parties imports
from sqlalchemy import create_engine
from sqlalchemy import text

# local imports
from tasktask.task import Task

def get_tasks(dbconf):
    '''Get and prepare the tasks'''
    # list of tasks
    tasks = []
    # get data to create the tasks
    connection = '{dbconnector}://{dbuser}:{dbpass}@{dbhost}/{dbname}'.format(dbconnector=dbconf['connector'],
                                                                            dbuser=dbconf['user'],
                                                                            dbpass=dbconf['pass'],
                                                                            dbhost=dbconf['host'],
                                                                            dbname=dbconf['name'])
    engine = create_engine(connection)
    sqlquery = dbconf['sql_query'].format(database = dbconf['name'])
    sql = text(sqlquery)
    sqlresult = engine.execute(sql)
    for line in sqlresult:
        tasks.append(Task({'id': line[0], 'schedule': line[1], 'created': line[2], 'command': line[3]}))
    return tasks
