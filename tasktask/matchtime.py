# Copyright © 2018 Carl Chenet <carl.chenet@ohmytux.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

'''is task execution time matching the current one'''

# standard library imports
import logging

# app library imports
import tasktask.matchtime

def match_time(now, schedule):
    '''is task execution time matching the current one'''
    is_today = False
    is_hour = False
    is_minute = False
    parameters = schedule.split('-')
    ###############################
    # everyday case
    ###############################
    if parameters[0] == 'everyday':
        is_today = True
        if len(parameters) == 1:
            parameters.extend(['at', '00', '00'])
        # check at and following arguments
        is_hour, is_minute = tasktask.matchtime.translate_at(parameters[2:], now)
    ###############################
    # every {hour, minute] cases
    ###############################
    elif parameters[0] == 'every':
        ###############################
        # every minute case
        ###############################
        if parameters[1] == 'minute':
            is_today = True
            is_hour = True
            is_minute = True
        elif parameters[1] == 'hour':
            is_today = True
            ###############################
            # every hour case
            ###############################
            if schedule == 'every-hour':
                parameters.extend(['at', '00', '00'])
            is_hour, is_minute = tasktask.matchtime.translate_at(parameters[3:], now)
    ##########################################################
    # check if all parameters are correct before returning
    ##########################################################
    if is_today and is_hour and is_minute:
        return True
    else:
        return False

def translate_at(parameters, now):
    '''check at and following arguments'''
    is_hour = False
    is_minute = False
    # 1st parameter is the hour
    if int(parameters[0]) == now.hour:
        is_hour = True
    # 2nd parameter is the minutes
    if int(parameters[1]) == now.minute:
        is_minute = True
    return is_hour, is_minute
